# symfony-test

1. Create a docker-compose file which will run 3 containers: symfony application, mysql database and nginx server.
1. Create a symfony default application with flex architecture inside it's docker container which will be accessible on http://localhost.
1. Create a user entity 
1. Create a data fixture to load an admin user in database
1. Create an admin page which must be granted by a login form with username & password.
1. Use bootstrap for UI.
1. Once done, please make a pull request here.